import React, { Component } from 'react';
import './App.css';
import { Card, Button, Image } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';


const githubURL = 'https://api.github.com/users/astephens91'

class App extends Component {
  constructor(props) {
    super(props);
    //Remember to set state to a singular object
    this.state = {
      isToggleOn: true,
      users: {}, 
      active: false 
    };

    // This binding is necessary to make `this` work in the callback
    this.handleToggle = this.handleToggle.bind(this);
  }
  

  //handleToggle function
  handleToggle() {
    //Performs GET request on githubURL
    fetch(githubURL)
      .then(res => res.json())
      .then(data => {
        this.setState({ users: data })
        console.log(data);
      });
    this.setState(state => ({ active: !state.active }));
  };


  render(){
    if (this.state.active) {
    return (<React.Fragment>
            <Button className="button"onClick={this.handleToggle}>
              Click This Butt
            </Button>
            <div className="content">
            <Card>
              <Image src= "https://avatars3.githubusercontent.com/u/47117972?v=4" />
              <br />
              Users:<div className="header">{this.state.users.id}</div>
              Name:<div className="header">{this.state.users.login}</div>
              Company:<div className="header">{this.state.users.company}</div>
              Location:<div className="header">{this.state.users.location}</div>
            </Card>
          </div>

            </React.Fragment>
    
    )
  } else {
    return (
      <React.Fragment>
      <Button onClick={this.handleToggle}>Click This Butt</Button>
      </React.Fragment>

    )
  }
}  
}

export default App;
